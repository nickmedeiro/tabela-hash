#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#define TAM 100000

long int v[TAM], v1[TAM], v2[TAM], v3[TAM+100];

int soma(long int vet[], long int tam){

    int i, soma = 0;

    for(i = 0; i<tam; ++i){
        soma+=vet[i];
    }

    return soma;

}

void inicia_vetor(long int v[], long int tam){

    int i;

    for(i = 0; i<tam; ++i){
        v[i] = -1;
    }

}

void inicia_vetor_zero(long int v[], long int tam){

    int i;

    for(i = 0; i<tam; ++i){
        v[i] = 0;
    }

}

int HEALP_MD(long int chave, long int M){

    int inc, pos, cont = 0;

    for(inc = 0; inc < M; ++inc){

        pos = (chave + inc) % M;
        if(v[pos] == -1 || v[pos] == chave){
            break;
        }else
            cont++;
    }

    assert (inc <= M);
    v[pos] = chave;

    return cont;

}

int HEAQP_MD(long int chave, long int M){

    int inc, pos, cont = 0;

    for(inc = 0; inc < M; ++inc){

        pos = (chave + inc*inc) % M;
        if(v1[pos] == -1 || v1[pos] == chave){
            break;
        }else
            cont++;
    }

    assert (inc <= M);
    v1[pos] = chave;

    return cont;

}

int HEALP_MM(long int chave, long int M){

    int inc, pos, cont = 0;

    for(inc = 0; inc < M; ++inc){

        pos = (chave * (int)((sqrt(5) - 1) / 2)) % M;
        if(v2[pos] == -1 || v2[pos] == chave){
            break;
        }else
            cont++;
    }

    assert (inc <= M);
    v2[pos] = chave;

    return cont;

}

int MEU_HEAP(long int chave,long int M){

    int inc, pos, cont = 0;

    for(inc = 0; inc < M; ++inc){

        pos = (chave * (int)((sqrt(5) - 1) / 2)) % M;
        if(v2[pos] == -1 || v2[pos] == chave){
            break;
        }else
            cont++;
    }

    assert (inc <= M);
    v3[pos] = chave;

    return cont;

}

void main(){

    unsigned int u;
    long int i, h1[TAM], h2[TAM], h3[TAM], h4[TAM+100];
    long int col1, col2, col3, col4, chave[TAM];
    FILE *arquivo1, *arquivo2, *arquivo3, *arquivo4;

    arquivo1 = fopen ("HEALPMD.txt","w+");
    arquivo2 = fopen ("HEAQPMD.txt","w+");
    arquivo3 = fopen ("HEALPMM.txt","w+");
    arquivo4 = fopen ("MEUHEAP.txt","w+");

    inicia_vetor(v, TAM);
    inicia_vetor(v1, TAM);
    inicia_vetor(v2, TAM);
    inicia_vetor(v3, TAM+100);

    inicia_vetor_zero(h1, TAM);
    inicia_vetor_zero(h2, TAM);
    inicia_vetor_zero(h3, TAM);
    inicia_vetor_zero(h4, TAM);

    printf("digite a semente do rand: ");
    scanf("%u", &u);
    srand(u);

    for (i = 0; i<TAM; ++i){
        chave[i] = rand()%RAND_MAX;
    }

    for(i = 0; i<TAM; ++i){
          h1[i] = HEALP_MD(chave[i], TAM);
          h2[i] = HEAQP_MD(chave[i], TAM);
          h3[i] = HEALP_MM(chave[i], TAM);
          h4[i] = MEU_HEAP(chave[i], TAM+100);
    }

    col1 = soma(h1, TAM);
    col2 = soma(h2, TAM);
    col3 = soma(h3, TAM);
    col4 = soma(h4, TAM);

    printf("HEALP MD %d colisoes\n", col1);
    printf("HEAQP MD %d colisoes\n", col2);
    printf("HEALP MM %d colisoes\n", col3);
    printf("MEU HEAP, O MELHOR %d colisoes\n", col4);

    for(i = 0; i<TAM; ++i){
        fprintf(arquivo1, " %d %d\n", chave[i], h1[i]);
        fprintf(arquivo2, " %d %d\n", chave[i], h2[i]);
        fprintf(arquivo3, " %d %d\n", chave[i], h3[i]);
        fprintf(arquivo4, " %d %d\n", chave[i], h4[i]);
    }

    fclose (arquivo1);
    fclose (arquivo2);
    fclose (arquivo3);
    fclose (arquivo4);

    system("pause");

}
